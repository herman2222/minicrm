@extends('layouts.app')

@section('content')
    <div>
        <section class="content">
            <div class="container">
                <div>
                    <h3>Departments List</h3>
                </div>
                <div>
                    <div class="form-group">
                        <a href="{{route('departments.create')}}" class="btn btn-success">Add New Department</a>
                    </div>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($departments as $department)
                            <tr>
                                <td>{{$department->id}}</td>
                                <td>{{$department->name}}</td>
                                <td>{{$department->email}}</td>
                                <td>{{$department->phone_number}}</td>
                                <td>
                                    <a href="{{route('departments.show', $department->id)}}" class="btn btn-primary">Show</a>
                                    <a href="{{route('departments.edit', $department->id)}}" class="btn btn-warning">Edit</a>

                                    {{Form::open(['route'=>['departments.destroy', $department->id], 'method'=>'delete', 'class' => 'delete-form-button'])}}
                                    <button onclick="return confirm('are you sure?')" type="submit" class="btn btn-danger">
                                        delete
                                    </button>

                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>

                    {{ $departments->links() }}
                </div>
            </div>
        </section>
    </div>
@endsection