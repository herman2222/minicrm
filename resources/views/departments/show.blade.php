@extends('layouts.app')

@section('content')
    <div>
        <section class="content">
            <div class="container">
                <div>
                    <h3 class="box-title">Department</h3>
                </div>
                <div>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$department->id}}</td>
                                <td>{{$department->name}}</td>
                                <td>{{$department->email}}</td>
                                <td>{{$department->phone_number}}</td>
                                <td>
                                    <a href="{{route('departments.edit', $department->id)}}" class="btn btn-warning">Edit</a>

                                    {{Form::open(['route'=>['departments.destroy', $department->id], 'method'=>'delete', 'class' => 'delete-form-button'])}}
                                    <button onclick="return confirm('are you sure?')" type="submit" class="btn btn-danger">
                                        delete
                                    </button>

                                    {{Form::close()}}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>
    </div>
@endsection