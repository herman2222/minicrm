@extends('layouts.app')

@section('content')

    <div class="container">
        <section class="content">
            {{Form::open([
                'route'	=>	['departments.store']
            ])}}
            <div>
                <div>
                    <h3 class="box-title">Create Department</h3>
                    @include('layouts.errors')
                </div>
                <div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" placeholder="" value="{{old('name')}}">
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control" name="email" placeholder="" value="{{old('email')}}">
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" name="phone_number" placeholder="" value="{{old('phone_number')}}">
                    </div>
                </div>
                <div>
                    <button class="btn btn-warning pull-right">Add</button>
                </div>
            </div>
            {{Form::close()}}
        </section>
    </div>

@endsection