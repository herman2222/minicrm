@extends('layouts.app')

@section('content')

    <div class="container">
        <section class="content">
        {{Form::open([
            'route'	=>	['departments.update', $department->id],
            'method'	=>	'put'
        ])}}
            <div>
                <div>
                    <h3 class="box-title">Edit Department</h3>
                    @include('layouts.errors')
                </div>
                <div>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" placeholder="" value="{{$department->name}}">
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="text" class="form-control" name="email" placeholder="" value="{{$department->email}}">
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" class="form-control" name="phone_number" placeholder="" value="{{$department->phone_number}}">
                        </div>
                </div>
                <div>
                    <button class="btn btn-warning pull-right">Save</button>
                </div>
            </div>
            {{Form::close()}}
        </section>
    </div>

@endsection