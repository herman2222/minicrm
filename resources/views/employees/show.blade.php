@extends('layouts.app')

@section('content')
    <div>
        <section class="content">
            <div class="container">
                <div>
                    <h3>Employee</h3>
                </div>
                <div>
                    <div class="form-group">
                        <a href="{{route('employees.create')}}" class="btn btn-success">Add new employee</a>
                    </div>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Department</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$employee->id}}</td>
                                <td>{{$employee->first_name}}</td>
                                <td>{{$employee->last_name}}</td>
                                <td>{{$employee->getDepartmentName()}}</td>
                                <td>{{$employee->email}}</td>
                                <td>{{$employee->phone_number}}</td>
                                <td>
                                    <a href="{{route('employees.edit', $employee->id)}}" class="btn btn-warning">Edit</a>

                                    {{Form::open(['route'=>['employees.destroy', $employee->id], 'method'=>'delete', 'class' => 'delete-form-button'])}}
                                    <button onclick="return confirm('are you sure?')" type="submit" class="btn btn-danger">
                                        delete
                                    </button>

                                    {{Form::close()}}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>
    </div>
@endsection