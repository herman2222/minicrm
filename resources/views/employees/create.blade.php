@extends('layouts.app')

@section('content')

    <div class="container">
        <section class="content">
            {{Form::open([
                'route'	=>	['employees.store']
            ])}}
            <div>
                <div>
                    <h3 class="box-title">Create employee</h3>
                    @include('layouts.errors')
                </div>
                <div>
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" name="first_name" placeholder="" value="{{old('first_name')}}">
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" name="last_name" placeholder="" value="{{old('last_name')}}">
                    </div>
                    <div class="form-group">
                        <label>Department Name</label>
                        {{Form::select('department_id',
                            $departments,
                            null,
                            ['class' => 'form-control select2'])
                        }}
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control" name="email" placeholder="" value="{{old('email')}}">
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" name="phone_number" placeholder="" value="{{old('phone_number')}}">
                    </div>
                </div>
                <div>
                    <button class="btn btn-warning pull-right">Add</button>
                </div>
            </div>
            {{Form::close()}}
        </section>
    </div>

@endsection