@extends('layouts.app')

@section('content')

    <div class="container">
        <section class="content">
            {{Form::open([
                'route'	=>	['employees.update', $employee->id],
                'method' => 'put'
            ])}}
            <div>
                <div>
                    <h3 class="box-title">Edit Employee</h3>
                    @include('layouts.errors')
                </div>
                <div>
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" name="first_name" placeholder="" value="{{$employee->first_name}}">
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" name="last_name" placeholder="" value="{{$employee->last_name}}">
                    </div>
                    <div class="form-group">
                        <label>Department Name</label>
                        {{Form::select('department_id',
                            $departments,
                            $employee->getDepartmentID(),
                            ['class' => 'form-control select2'])
                        }}
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control" name="email" placeholder="" value="{{$employee->email}}">
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" name="phone_number" placeholder="" value="{{$employee->phone_number}}">
                    </div>
                </div>
                <div>
                    <button class="btn btn-warning pull-right">Save</button>
                </div>
            </div>
            {{Form::close()}}
        </section>
    </div>

@endsection