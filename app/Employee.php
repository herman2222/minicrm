<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone_number', 'department_id'
    ];

    public function department() {
        return $this->belongsTo(Department::class);
    }


    public function getDepartmentName()
    {
        return ($this->department != null)
            ?   $this->department->name
            :   '';
    }

    public function setDepartment($id)
    {
        if($id == null) {return;}
        $this->department_id = $id;
        $this->save();
    }

    public function getDepartmentID()
    {
        return $this->department() != null ? $this->department->id : null;
    }

    public static function add($fields) {
        $employee = new static;
        $employee->fill($fields);
        $employee->save();

        return $employee;

    }

}
