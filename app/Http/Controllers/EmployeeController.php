<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use LVR\Phone\E164;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::paginate(10);
        return view('employees.index', ['employees'   =>  $employees]);
    }

    public function show($id) {
        $employee = Employee::find($id);
        return view('employees.show', compact('employee'));
    }

    public function create()
    {
        $departmentList = Department::all();
        $departments = $departmentList->pluck('name', 'id');
        $departments->all();


        return view('employees.create', compact('departments'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'  =>  'required',
            'last_name'  =>  'required',
            'email' =>  'nullable|email|unique:employees',
            'phone_number' => ['nullable', new E164],
        ]);

        $employee = Employee::add($request->all());
        $employee->setDepartment($request->get('department_id'));

        return redirect()->route('employees.index');
    }

    public function edit($id)
    {
        $employee = Employee::find($id);
        $departmentList = Department::all();
        $departments = $departmentList->pluck('name', 'id');
        $departments->all();
        return view('employees.edit', compact('employee', 'departments'));
    }

    public function update(Request $request, $id)
    {
        $employee = Employee::find($id);

        $this->validate($request, [
            'first_name'  =>  'required',
            'last_name'  =>  'required',
            'email' =>  ['nullable','email',
                        Rule::unique('employees')->ignore($employee->id)],
            'phone_number' => ['nullable', new E164],
        ]);

        $employee->fill($request->all());
        $employee->save();
        $employee->setDepartment($request->get('department_id'));

        return redirect()->route('employees.index');
    }

    public function destroy($id)
    {
        Employee::find($id)->delete();

        return redirect()->route('employees.index');
    }
}
