<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use LVR\Phone\E164;

class DepartmentController extends Controller
{
    public function index()
    {
        $departments = Department::paginate(10);
        return view('departments.index', ['departments'   =>  $departments]);
    }

    public function show($id) {
        $department = Department::find($id);
        return view('departments.show', compact('department'));
    }

    public function create()
    {
        return view('departments.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  =>  'required',
            'email' =>  'nullable|email|unique:departments',
            'phone_number' => ['nullable', new E164],
        ]);

        $department = Department::add($request->all());
        
        return redirect()->route('departments.index');
    }

    public function edit($id)
    {
        $department = Department::find($id);
        return view('departments.edit', compact('department'));
    }

    public function update(Request $request, $id)
    {
        $department = Department::find($id);

        $this->validate($request, [
            'name'  =>  'required',
            'email' =>  ['nullable', 'email',
                        Rule::unique('departments')->ignore($department->id)
            ],
            'phone_number' => ['nullable', new E164],
        ]);

        $department->fill($request->all());
        $department->save();

        return redirect()->route('departments.index');
    }

    public function destroy($id)
    {
        Department::find($id)->delete();

        return redirect()->route('departments.index');
    }
}
