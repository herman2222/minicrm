<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name', 'email', 'phone_number'
    ];

    public function employees() {
        return $this->hasMany(Employee::class);
    }

    public static function add($fields) {
        $department = new static;
        $department->fill($fields);
        $department->save();

        return $department;

    }
}
