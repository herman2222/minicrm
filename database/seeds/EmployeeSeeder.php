<?php

use Illuminate\Database\Seeder;
use App\Department;
use App\Employee;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::all()->each(function (Department $department) {
            $employees = factory(Employee::class, random_int(2,5))->make();
            $department->employees()->saveMany($employees);
        });
    }
}
