<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Department;
use Faker\Generator as Faker;

$factory->define(Department::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->email,
        'phone_number' => $faker->e164PhoneNumber
    ];
});
